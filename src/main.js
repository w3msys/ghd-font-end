// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueAxios from 'vue-axios'
import Axios from './config/http'
import Auth from './config/auth'
import store from './store'
import VeeValidate from 'vee-validate'
import plugins from './plugins'

Vue.config.productionTip = false
Vue.use(VueAxios, Axios)
Vue.use(Auth)
Vue.use(VeeValidate)
Vue.use(plugins)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
