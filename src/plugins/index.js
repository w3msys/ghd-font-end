import api from './api'

export default (Vue) => {
    Vue.use(api)
}