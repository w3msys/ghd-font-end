import axios from './../config/http';
import store from './../store'

export default {
    install (Vue) {
        Vue.api = {

            getUser () {
                axios({
                    method: 'get',
                    url: 'me'
                }).then(response => {
                    // console.log('user', response.data)
                    store.dispatch('setUser', response.data)
                })
            },

            getHotels () {
                axios({
                    method: 'get',
                    url: 'hotels'
                }).then(response => {
                    store.dispatch('setHotels', response.data || [])
                })
            },

            getManagers () {
                axios({
                    method: 'get',
                    url: 'managers'
                }).then(response => {
                    store.dispatch('setManagers', response.data || [])
                })
            }
        }

        Vue.prototype.$api = Vue.api;
    }
}