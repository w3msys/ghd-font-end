import Vue from 'vue'
import Router from 'vue-router'
import Login from '../components/authentication/Login.vue'
import Auth from '../components/layouts/Auth.vue'
import Dashboard from '../components/dashboard/dashboard.vue'
import Hotels from '../components/hotels/hotels.vue'
import Managers from '../components/managers/managers.vue'

Vue.use(Router)

const router = new Router({
  // mode: 'histhory',
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/auth',
      name: 'Auth',
      component: Auth,
      children: [
        {
          name: 'Dashboard',
          path: 'dashboard',
          component: Dashboard
        },
        {
          name: 'Hotels',
          path: 'hotels',
          component: Hotels,
          props: true
        },
        {
          name: 'Managers',
          path: 'managers',
          component: Managers,
          props: true
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.name === 'Login') {
    Vue.auth.destoryToken();
  }

  $('title').text(`GHD | ${to.name}`)
  next()
})

export default router
