import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {},
    hotels: [
      // { id: 1, name: 'La Palm Hotel'}, {id: 2, name: 'Las Palmas Hotel'}, {id: 3, name: 'Golden Tulip Hotel'}
    ],
    managers: []
  },
  getters: {
    getUser (state) {
      return state.user
    },
    getHotels (state) {
      return state.hotels
    },
    getManagers (state) {
      return state.managers
    }
  },
  mutations: {
    CLEAR_STATE (state) {
      state.user = {}
      state.managers = []
      state.hotels = []
    },
    SET_USER (state, payload) {
      state.user = payload
    },
    SET_HOTELS (state, payload) {
      state.hotels = payload
    },
    SET_MANAGERS (state, payload) {
      state.managers = payload
    },
    ADD_HOTEL (state, payload) {
      state.hotels.unshift(payload)
    },
    ADD_MANAGER (state, payload) {
      state.managers.unshift(payload)
    },
    REMOVE_MANAGER (state, payload) {
      for (let i = 0; i < state.managers.length; i++) {
        if (state.managers[i].id === payload) {
          state.managers.splice(i, 1)
          return
        }
      }
    },
    REMOVE_HOTEL (state, payload) {
      for (let i = 0; i < state.hotels.length; i++) {
        if (state.hotels[i].id === payload) {
          state.hotels.splice(i, 1)
          return
        }
      }
    },
    EDIT_HOTEL (state, payload) {
      for (let i = 0; i < state.hotels.length; i++) {
        if (state.hotels[i].id === payload) {
          state.hotels[i] = payload
          return
        }
      }
    },
    EDIT_MANAGER (state, payload) {
      for (let i = 0; i < state.managers.length; i++) {
        if (state.managers[i].id === payload) {
          state.managers[i] = payload
          return
        }
      }
    }
  },
  actions: {
    clearState ({commit}) {
      commit('CLEAR_STATE')
    },
    initStore ({commit}, payload) {

      // commit('SET_USER', payload.user)
      // commit('SET_HOTELS', payload.hotels)
      // commit('SET_MANAGERS', payload.managers)
    },
    setUser ({commit}, payload) {
      commit('SET_USER', payload)
    },
    setHotels ({commit}, payload) {
      commit('SET_HOTELS', payload)
    },
    setManagers ({commit}, payload) {
      commit('SET_MANAGERS', payload)
    },
    addHotel ({commit}, payload) {
      commit('ADD_HOTEL', payload)
    },
    addManager ({commit}, payload) {
      commit('ADD_MANAGER', payload)
    },
    removeManager ({commit}, payload) {
      commit('REMOVE_MANAGER', payload)
    },
    removeHotel ({commit}, payload) {
      commit('REMOVE_HOTEL', payload)
    },
    editHotel ({commit}, payload) {
      commit('EDIT_HOTEL', payload)
    },
    editManager ({commit}, payload) {
      commit('EDIT_MANAGER', payload)
    }
  }
})
