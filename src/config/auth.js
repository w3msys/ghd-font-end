export default (Vue) => {
  Vue.auth = {
    getToken () {
      const token = sessionStorage.getItem('ghd-token')
      // const expiration = localStorage.getItem('expiration')

      if (!token) {
        return null
      }
      return token
    },
    setToken (token) {
      sessionStorage.setItem('ghd-token', token)
      // localStorage.setItem('expiration', expiration)
    },
    destoryToken () {
      sessionStorage.removeItem('ghd-token')
      // localStorage.removeItem('expiration')
    },
    isAuthenticated () {
      if (this.getToken()) {
        return true
      } else {
        return false
      }
    }
  }

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get () {
        return Vue.auth
      }
    }
  })
}
