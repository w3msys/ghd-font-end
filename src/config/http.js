import Axios from 'axios'
import Vue from 'vue'

const instance = Axios.create({
  baseURL: 'http://ghd.test/api/',
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
})

instance.interceptors.request.use((config) => {
  let apiToken = Vue.auth.getToken()
  if (apiToken && !config.headers.common.Authorization) {
    config.headers.common.Authorization = `Bearer ${apiToken}`
  }
  return config
})

export default instance
